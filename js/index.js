var dsnv = [];

// Parse JSON + Map new arr để lấy được func của các object
var dsnvJSON = localStorage.getItem("dsnv");

if(dsnvJSON != null){
    var nvArr = JSON.parse(dsnvJSON);
    dsnv = nvArr.map(function(item){
        return new NhanVien(item.taiKhoan, item.hoVaTen, item.email, item.matKhau, item.ngay, item.luongCoBan, item.chucVu, item.gioLam);
    })
    renderLayout(dsnv);
}

// Func: thêm nhân viên
function themNhanVien(){
    // lấy thông tin từ form
    var nv = layThongTinTuForm();

    // validate
    var isValid = validateNV(nv);
    if(isValid){
        //push vào arr và render layout
        dsnv.push(nv);
        renderLayout(dsnv);
        resetForm();
        alert("Thêm người dùng thành công!");

        // gửi thông tin lên localStorage
        var dsnvJSON = JSON.stringify(dsnv);
        localStorage.setItem("dsnv",dsnvJSON);
    }
}

// Func: xóa nhân viên
function xoaNhanVien(tk){
    var viTri = timViTri(tk,dsnv)
    
    dsnv.splice(viTri,1);
    renderLayout(dsnv);

    var dsnvJSON = JSON.stringify(dsnv);
    localStorage.setItem("dsnv",dsnvJSON);
}

// Func: sửa nhân viên (show thông tin)
function suaNhanVien(tk){
    var viTri = timViTri(tk,dsnv);

    // show thông tin nhân viên
    var item = dsnv[viTri];

    document.getElementById("tknv").value = item.taiKhoan;
    document.getElementById("name").value = item.hoVaTen;
    document.getElementById("email").value = item.email;
    document.getElementById("password").value = item.matKhau;
    document.getElementById("datepicker").value = item.ngay;
    document.getElementById("luongCB").value = item.luongCoBan;
    document.getElementById("chucvu").value = item.chucVu;
    document.getElementById("gioLam").value = item.gioLam;

    document.getElementById("tknv").setAttribute("disabled", "");
    document.getElementById("btnThemNV").style.display = "none";
    resetFormSpan()
}

// Func: cập nhật nhân viên
function capNhatNhanVien(){
    var nv = layThongTinTuForm();
    var viTri = timViTri(nv.taiKhoan,dsnv);

    var isValid = true;
    isValid = validateNVCapNhat(nv);

    if(isValid){
        dsnv[viTri] = nv;
        renderLayout(dsnv);
        resetForm();
        alert("Cập nhật thành công!");

        var dsnvJSON = JSON.stringify(dsnv);
        localStorage.setItem("dsnv",dsnvJSON);
    }
}

// Func: tìm kiếm
$("#searchName").keyup(function(event) {
    if (event.keyCode === 13) {
        $("#btnTimNV").click();
    }
});

$("#btnTimNV").click(function() {
    var dataSearch = document.getElementById("searchName").value;
    var dataXuatSac = dataXepLoai(dsnv,"Xuất Sắc");
    var dataGioi = dataXepLoai(dsnv,"Giỏi");
    var dataKha = dataXepLoai(dsnv,"Khá");
    var dataTrungBinh = dataXepLoai(dsnv,"Trung bình");
    var dataRong = []
    
    if(dataSearch == "Giỏi" || dataSearch == "giỏi"){
        renderLayout(dataGioi);
    } else if(dataSearch == "Xuất sắc" || dataSearch == "xuất sắc"){
        renderLayout(dataXuatSac);
    } else if(dataSearch == "Khá" || dataSearch == "khá"){
        renderLayout(dataKha);
    } else if(dataSearch == "Trung bình" || dataSearch == "trung bình"){
        renderLayout(dataTrungBinh);
    } else{
        renderLayout(dataRong);
    }
    
});