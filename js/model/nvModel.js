function NhanVien(
    _taiKhoan,
    _hoVaTen,
    _email,
    _matKhau,
    _ngay,
    _luongCoBan,
    _chucVu,
    _gioLam
){
    this.taiKhoan = _taiKhoan;
    this.hoVaTen = _hoVaTen;
    this.email = _email;
    this.matKhau = _matKhau;
    this.ngay = _ngay;
    this.luongCoBan = _luongCoBan;
    this.chucVu = _chucVu;
    this.gioLam = _gioLam;
    this.tongLuong = function(){
        var tongLuong = 0;
        if(this.chucVu == "Sếp"){
            tongLuong = this.luongCoBan * 3;
        } else if(this.chucVu == "Trưởng phòng"){
            tongLuong = this.luongCoBan * 2;
        } else if(this.chucVu == "Nhân viên"){
            tongLuong = this.luongCoBan;
        }
        return tongLuong;
    }
    this.xepLoai = function(){
        var xepLoai = ""
        if(this.gioLam >= 192){
            xepLoai = "Xuất sắc";
        } else if(this.gioLam >=176){
            xepLoai = "Giỏi";
        } else if(this.gioLam >=160){
            xepLoai = "Khá";
        } else if(this.gioLam < 160 && this.gioLam >=80){
            xepLoai = "Trung bình";
        }
        return xepLoai;
    }
}