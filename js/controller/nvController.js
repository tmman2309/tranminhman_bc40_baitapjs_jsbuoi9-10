// lấy thông tin từ form
function layThongTinTuForm(){
    var taiKhoan = document.getElementById("tknv").value;
    var hoVaTen = document.getElementById("name").value;
    var email = document.getElementById("email").value;
    var matKhau = document.getElementById("password").value;
    var ngay = document.getElementById("datepicker").value;
    var luongCoBan = document.getElementById("luongCB").value *1;
    var chucVu = document.getElementById("chucvu").value;
    var gioLam = document.getElementById("gioLam").value *1;

    var nv = new NhanVien(taiKhoan,hoVaTen,email,matKhau,ngay,luongCoBan,chucVu,gioLam);

    return nv;
}

 // renderLayout
function renderLayout(dsnv){
    var contentTable = "";
    for (let index = 0; index < dsnv.length; index++) {
        var item = dsnv[index];
        var contentHTML = `
                <tr>
                    <th>${item.taiKhoan}</th>
                    <th>${item.hoVaTen}</th>
                    <th>${item.email}</th>
                    <th>${item.ngay}</th>
                    <th>${item.chucVu}</th>
                    <th>${new Intl.NumberFormat('de-DE').format(item.tongLuong())}</th>
                    <th>${item.xepLoai()}</th>
                    <th>
                        <button onclick="xoaNhanVien('${item.taiKhoan}')" class="btn btn-danger">Xóa</button>
                        <button onclick="suaNhanVien('${item.taiKhoan}')" class="btn btn-warning" data-toggle="modal" data-target="#myModal">Sửa</button>
                    </th>
                </tr>
        `
        contentTable += contentHTML;
    }
    document.getElementById("tableDanhSach").innerHTML = contentTable;
}

// reset form
function resetForm(){
    document.getElementById("tknv").value = "";
    document.getElementById("name").value = "";
    document.getElementById("email").value = "";
    document.getElementById("password").value = "";
    document.getElementById("datepicker").value = "";
    document.getElementById("luongCB").value = "";
    document.getElementById("chucvu").value = "ccv";
    document.getElementById("gioLam").value = "";

    document.getElementById("tbTKNV").innerText = "";
    document.getElementById("tbTen").innerText = "";
    document.getElementById("tbEmail").innerText = "";
    document.getElementById("tbMatKhau").innerText = "";
    document.getElementById("tbNgay").innerText = "";
    document.getElementById("tbLuongCB").innerText = "";
    document.getElementById("tbChucVu").innerText = "";
    document.getElementById("tbGiolam").innerText = "";

    document.getElementById("tknv").removeAttribute("disabled", "");
    document.getElementById("btnThemNV").style.display = "block";
}

function resetFormSpan(){
    document.getElementById("tbTKNV").innerText = "";
    document.getElementById("tbTen").innerText = "";
    document.getElementById("tbEmail").innerText = "";
    document.getElementById("tbMatKhau").innerText = "";
    document.getElementById("tbNgay").innerText = "";
    document.getElementById("tbLuongCB").innerText = "";
    document.getElementById("tbChucVu").innerText = "";
    document.getElementById("tbGiolam").innerText = "";
}

// validate
function validateNV(nv){
    var isValid = true;

    var isValidTK = kiemTraBoTrong(nv.taiKhoan,"tbTKNV") && kiemTraSoLuong(nv.taiKhoan,"tbTKNV",4,6) && kiemTraTrungTaiKhoan(nv.taiKhoan,dsnv);
    var isValidHVT = kiemTraBoTrong(nv.hoVaTen,"tbTen") && kiemTraString(nv.hoVaTen,"tbTen");
    var isValidEmail = kiemTraBoTrong(nv.email,"tbEmail") && kiemTraEmail(nv.email,"tbEmail") && kiemTraTrungEmail(nv.email,dsnv);
    var isValidMK = kiemTraBoTrong(nv.matKhau,"tbMatKhau") && kiemTraSoLuong(nv.matKhau,"tbMatKhau",6,10) && kiemTraMatKhau(nv.matKhau,"tbMatKhau");
    var isValidNgay = kiemTraBoTrong(nv.ngay,"tbNgay");
    var isValidLuongCB = kiemTraBoTrong(nv.luongCoBan,"tbLuongCB") && kiemTraSoLuongNumber1(nv.luongCoBan,"tbLuongCB",1000000,20000000) && kiemTraNumber(nv.luongCoBan,"tbLuongCB");
    var isValidChucVu = kiemTraBoTrongChucVu(nv.chucVu,"tbChucVu");
    var isValidGioLam = kiemTraBoTrong(nv.gioLam,"tbGiolam") && kiemTraSoLuongNumber2(nv.gioLam,"tbGiolam",80,200) && kiemTraNumber(nv.gioLam,"tbGiolam");

    isValid = isValidTK & isValidHVT & isValidEmail & isValidMK & isValidNgay & isValidLuongCB & isValidChucVu & isValidGioLam;

    return isValid;
}

function validateNVCapNhat(nv){
    var isValid = true;

    var isValidTK = kiemTraBoTrong(nv.taiKhoan,"tbTKNV") && kiemTraSoLuong(nv.taiKhoan,"tbTKNV",4,6);
    var isValidHVT = kiemTraBoTrong(nv.hoVaTen,"tbTen") && kiemTraString(nv.hoVaTen,"tbTen");
    var isValidEmail = kiemTraBoTrong(nv.email,"tbEmail") && kiemTraEmail(nv.email,"tbEmail");
    var isValidMK = kiemTraBoTrong(nv.matKhau,"tbMatKhau") && kiemTraSoLuong(nv.matKhau,"tbMatKhau",6,10) && kiemTraMatKhau(nv.matKhau,"tbMatKhau");
    var isValidNgay = kiemTraBoTrong(nv.ngay,"tbNgay");
    var isValidLuongCB = kiemTraBoTrong(nv.luongCoBan,"tbLuongCB") && kiemTraSoLuongNumber1(nv.luongCoBan,"tbLuongCB",1000000,20000000) && kiemTraNumber(nv.luongCoBan,"tbLuongCB");
    var isValidChucVu = kiemTraBoTrongChucVu(nv.chucVu,"tbChucVu");
    var isValidGioLam = kiemTraBoTrong(nv.gioLam,"tbGiolam") && kiemTraSoLuongNumber2(nv.gioLam,"tbGiolam",80,200) && kiemTraNumber(nv.gioLam,"tbGiolam");

    isValid = isValidTK & isValidHVT & isValidEmail & isValidMK & isValidNgay & isValidLuongCB & isValidChucVu & isValidGioLam;

    return isValid;
}

// Tìm vị trí index của object
function timViTri(tk, dsnv){
    var index = dsnv.findIndex(function(item){
        return tk == item.taiKhoan;
    })

    if(index != -1){
        return index;
    }
}

// Filter data xếp loại
function dataXepLoai(dsnv,loaiNV){
    var data = [];

    for (let index = 0; index < dsnv.length; index++) {
        var item = dsnv[index];
        if(item.xepLoai() == loaiNV){
            data.push(item);
        }
    }

    return data;
}
