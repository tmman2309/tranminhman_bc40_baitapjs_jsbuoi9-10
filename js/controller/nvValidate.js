// Kiểm tra bỏ trống
function kiemTraBoTrong(idString, idErr){
    if(idString.length == 0 || idString == 0){
        document.getElementById(idErr).style.display = "block";
        document.getElementById(idErr).innerHTML = `<i>*bắt buộc điền</i>`;
        return false;
    } else{
        document.getElementById(idErr).style.display = "none";
        document.getElementById(idErr).innerHTML = "";
        return true;
    }
}

function kiemTraBoTrongChucVu(idString, idErr){
    if(idString == "ccv"){
        document.getElementById(idErr).style.display = "block";
        document.getElementById(idErr).innerHTML = `<i>*bắt buộc chọn</i>`;
        return false;
    } else{
        document.getElementById(idErr).style.display = "none";
        document.getElementById(idErr).innerHTML = "";
        return true;
    }
}

// Kiểm tra số lượng kí tự
function kiemTraSoLuong(idString, idErr, min, max){
    if(idString.length < min || idString.length > max){
        document.getElementById(idErr).style.display = "block";
        document.getElementById(idErr).innerHTML = `<i>*Độ dài từ ${min} đến ${max} kí tự</i>`;
        return false;
    } else{
        document.getElementById(idErr).style.display = "none";
        document.getElementById(idErr).innerHTML = "";
        return true;
    }
}

function kiemTraSoLuongNumber1(idString, idErr, min, max){
    if(idString < min || idString > max){
        document.getElementById(idErr).style.display = "block";
        document.getElementById(idErr).innerHTML = `<i>*Lương cơ bản từ 1.000.000 - 20.000.000`;
        return false;
    } else{
        document.getElementById(idErr).style.display = "none";
        document.getElementById(idErr).innerHTML = "";
        return true;
    }
}

function kiemTraSoLuongNumber2(idString, idErr, min, max){
    if(idString < min || idString > max){
        document.getElementById(idErr).style.display = "block";
        document.getElementById(idErr).innerHTML = `<i>*Số giờ làm trong tháng từ 80 - 200 giờ`;
        return false;
    } else{
        document.getElementById(idErr).style.display = "none";
        document.getElementById(idErr).innerHTML = "";
        return true;
    }
}

// Kiểm tra Regex
function kiemTraString(idString, idErr){
    var res = /^[a-zA-Z]*$/;
    if(res.test(idString)){
        document.getElementById(idErr).style.display = "none";
        document.getElementById(idErr).innerHTML = "";
        return true;
    } else{
        document.getElementById(idErr).style.display = "block";
        document.getElementById(idErr).innerHTML = `<i>*Trường này phải là chữ và không có dấu cách!`;
        return false;
    }
}

function kiemTraNumber(idString, idErr){
    var reg = /^\d+$/;
    if(reg.test(idString)){
        document.getElementById(idErr).style.display = "none";
        document.getElementById(idErr).innerHTML = "";
        return true;
    } else{
        document.getElementById(idErr).style.display = "block";
        document.getElementById(idErr).innerHTML = `<i>*Trường này phải là số!`;
        return false;
    }
}

function kiemTraEmail(idString, idErr){
    const re =
  /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
    if(re.test(idString)){
        document.getElementById(idErr).style.display = "none";
        document.getElementById(idErr).innerHTML = "";
        return true;
    } else{
        document.getElementById(idErr).style.display = "block";
        document.getElementById(idErr).innerHTML = `<i>*Email không hợp lệ!`;
        return false;
    }
}

function kiemTraMatKhau(idString, idErr){
    const reMK =  /^(?=.*[0-9])(?=.*[!@#$%^&*])(?=.*[A-Z])[a-zA-Z0-9!@#$%^&*]{6,10}$/;

    if(reMK.test(idString)){
        document.getElementById(idErr).style.display = "none";
        document.getElementById(idErr).innerHTML = "";
        return true;
    } else{
        document.getElementById(idErr).style.display = "block";
        document.getElementById(idErr).innerHTML = `<i>*Mật khẩu chứa ít nhất 1 ký tự số, 1 ký tự in hoa, 1 ký tự đặc biệt`;
        return false;
    }
}

// Kiểm tra trùng
function kiemTraTrungTaiKhoan(idString, dsnv){
    var index = dsnv.findIndex(function(item){
        return idString == item.taiKhoan;
    })

    if(index != -1){
        document.getElementById("tbTKNV").style.display = "block";
        document.getElementById("tbTKNV").innerHTML = `<i>*Tài khoản đã tồn tại!`;
        return false;
    } else{
        document.getElementById("tbTKNV").style.display = "none";
        document.getElementById("tbTKNV").innerHTML = "";
        return true;
    }
}

function kiemTraTrungEmail(idString, dsnv){
    var index = dsnv.findIndex(function(item){
        return idString == item.email;
    })

    if(index != -1){
        document.getElementById("tbEmail").style.display = "block";
        document.getElementById("tbEmail").innerHTML = `<i>*Email đã được sử dụng!`;
        return false;
    } else{
        document.getElementById("tbEmail").style.display = "none";
        document.getElementById("tbEmail").innerHTML = "";
        return true;
    }
}